var oDoc;
function init(){
  $("#accordion").accordion({
    heightStyle: "fill"
  });

  // initialisation de l'arbre
  $('#tree').jstree({
                      'core' : {
                      		"animation" : 0,
                      		"check_callback" : true,
                      		"themes" : { "stripes" : true },
                              'data' : {"cache":false,"url" : "./root.json", "dataType" : "json" }// needed only if you do not supply JSON headers
                                },
                      "types" : {
                      			"#" : { "max_children" : 1, "max_depth" : 4, "valid_children" : ["root"] },
                     				 "root" : { 'icon':"./icons/folder.png", "valid_children" : ["default"] },
                      			"default" : { 'icon':"./icons/folder.png","valid_children" : ["default","file"] },
                      			"file" : { 'icon' :"./icons/blog.png", "valid_children" : [] }
                      			},
                      "plugins" : [ "contextmenu", "dnd", "state", "types", "wholerow"],
                      "contextmenu":{ "items": createmenu}
                    });

  		// On affiche le nom du fichier sélectionné dans une boite de dialogue
  		$('#tree').on("select_node.jstree",function(e,data)
  		{
  			// si le noeud représente un fichier
  			if (data.node.type=='file')
  			{
  				alert("fichier "+data.node.text+" sélectionné");
  			}
  		});


  var tabs = $( "#tabs" ).tabs();
  // close icon: removing the tab on click
  tabs.delegate( "span.ui-icon-close", "click", function() {
    var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
    $( "#" + panelId ).remove();
    tabs.tabs( "refresh" );
  });

  // Initialise oDoc, l'element HTML sur lequel portera execCommand
  oDoc = document.getElementById("textBox");
}

// Applique la commande sCmd sur la sÃ©lection de l'element oDoc via execCommand
function formatDoc(sCmd) {
	oDoc.focus();
	document.execCommand(sCmd, false, null);//execCommand permet les commandes, modifier les contenus éditables de la page
  //utilisé pour italic, bold... arg : commande, showdefaultUI or not, an arg for the cmd.
}

$('#save').click(function(){
  $.post("save.php",{text: oDoc.innerHTML},function(data, status){
    if(status){//
      alert("Fichier sauvegardé");
    }
  });
});



// création du menu contextuel ouvert sur clic droit sur node
	function createmenu(node)
	{
        var tree = $("#tree").jstree(true);
        return {
                    "item1":
                    {
                        "label": "Créer un Dossier",
                        "action":function ()
                        {
                            node = tree.create_node(node);
                            tree.edit(node);
                        }
                    },

                    "item2":
                    {
                        "label": "Créer un Fichier",
                        "action": function ()
                        {
                             node = tree.create_node(node,{"type":"file"});
                              tree.edit(node);
                        }
                    },

                    "item3":
                    {
                		"label": "Renommer",
                		"action": function (obj)
                		{
                			tree.edit(node);
                		}
            		},

            		"item4":
            		{
                		"label": "Supprimer",
                		"action": function (obj)
                		{
                			tree.delete_node(node);
                		}
            		}
        		};
    }
